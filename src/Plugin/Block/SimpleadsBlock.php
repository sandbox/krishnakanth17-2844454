<?php

namespace Drupal\simpleads\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Provides a 'Simpleads' Block
 *
 * @Block(
 *   id = "simpleads_block",
 *   admin_label = @Translation("Simpleads"),
 *   category = @Translation("Simple Advertisement Block"),
 * )
 */
class SimpleadsBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();
    // Add a form field to the existing block configuration form.


    $form['ad_type'] = array(
      '#type' => 'radios',
      '#title' => t('Ad Type'),
      '#options' => array('image' => t('Image Ad'), 'text' => t('Text Ad'), 'flash' => t('Flash Ad')),
      '#default_value' => isset($config['ad_type'])? $config['ad_type'] : '',
    );
    $form['image'] = array(
      '#type' => 'managed_file',
      '#title' => t('Ad Image'),
      '#description' => t('Upload an image for this Ad.' . "<br>" .'Files must be less than 2 MB.' . "<br>" .  'Allowed file types:' . "<b>" . 'png gif jpg jpeg.' . "</b>"),
      '#attributes' => array('enctype' => "multipart/form-data"),
      '#default_value' => isset($config['image'])? $config['image'] : '',
      '#upload_location' => 'public://images/',
      '#upload_validators'  => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
        'file_validate_size' => array(25600000),
      ),
    );
    $form['url'] = array(
      '#type' => 'url',
      '#title' => t('URL Address'),
      '#default_value' => isset($config['url'])? $config['url'] : '',
      '#states' => array(
        'visible' => array(
          ':input[name="settings[ad_type]"]' => array('value' => 'image'),
        ),
      ),
    );
    $form['text'] = array(
      '#type' => 'text_format',
      '#title' => t('Text Ad'),
      '#description' => t('Enter your text ad.'),
      '#format' => 'full_html',
      '#default_value' => isset($config['text']['value'])? $config['text']['value'] : '',
      '#states' => array(
        'visible' => array(
          array(':input[name="settings[ad_type]"]' => array('value' => 'text')),
        ),
      ),
    );
    $form['flash'] = array(
      '#type' => 'managed_file',
      '#title' => t('Ad Flash'),
      '#description' => t('Upload an image for this Ad.' . "<br>" .'Files must be less than 2 MB.' . "<br>" .  'Allowed file types:' . "<b>" . 'swf.' . "</b>"),
      '#attributes' => array('enctype' => "multipart/form-data"),
      '#default_value' => isset($config['flash'])? $config['flash'] : '',
      '#upload_location' => 'public://images/',
      '#upload_validators'  => array(
        'file_validate_extensions' => array('swf'),
        'file_validate_size' => array(25600000),
      ),
      '#states' => array(
        'visible' => array(
        ),
      ),
    );
    $form['active_date'] = array(
      '#type' => 'date',
      '#title' => t('Active Date'),
      '#required' => FALSE,
      '#default_value' => isset($config['active_date'])? $config['active_date'] : '',
    );
    $form['end_date'] = array(
      '#type' => 'date',
      '#title' => t('End Date'),
      '#required' => FALSE,
      '#default_value' => isset($config['end_date'])? $config['end_date'] : '',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    //Validation for empty fields
    if($form_state->getValue('ad_type') == '') {
      $form_state->setErrorByName('ad_type', $this->t('Select Ad Type'));
    } else {
      if($form_state->getValue('ad_type') == 'image'){
        if($form_state->getValue('image')[0] == '') {
          $form_state->setErrorByName('image', $this->t('Upload Ad Image'));
        } elseif($form_state->getValue('url') == '') {
          $form_state->setErrorByName('url', $this->t('Enter URL Address'));
        }
      } elseif ($form_state->getValue('ad_type') == 'text') {
        if($form_state->getValue('text')['value'] == '') {
          $form_state->setErrorByName('text', $this->t('Enter Ad text'));
        }
      } elseif ($form_state->getValue('ad_type') == 'flash') {
        if($form_state->getValue('flash')[0] == '') {
          $form_state->setErrorByName('flash', $this->t('Upload Flash File'));
        }
      }
      //validation for Active Date and End Date
      $active_date = strtotime($form_state->getValue('active_date'));
      $end_date = strtotime($form_state->getValue('end_date'));

      if($form_state->getValue('end_date') != '' ) {
        $current_date = strtotime(date("Y-m-d"));
        if ($end_date < $current_date) {
          $form_state->setErrorByName('end_date', 'End date must be in the current or future');
        }
        if ($active_date > $end_date) {
          $form_state->setErrorByName('end_date', 'Active Date should be less than End Date');
        }
      }
    }

    return $form_state;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $this->setConfigurationValue('ad_type', $form_state->getValue('ad_type'));
    $this->setConfigurationValue('image', $form_state->getValue('image'));
    $this->setConfigurationValue('url', $form_state->getValue('url'));
    $this->setConfigurationValue('flash', $form_state->getValue('flash'));
    $this->setConfigurationValue('text', $form_state->getValue('text'));
    $this->setConfigurationValue('active_date', $form_state->getValue('active_date'));
    $this->setConfigurationValue('end_date', $form_state->getValue('end_date'));
  }

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    $config = $this->getConfiguration();
    $ad_type = isset($config['ad_type']) ? $config['ad_type'] : '';
    $image_fid = isset($config['image']) ? $config['image'] : '';
    $url = isset($config['url']) ? $config['url'] : '';
    $text = isset($config['text']) ? $config['text'] : '';
    $flash_fid = isset($config['flash']) ? $config['flash'] : '';
    $active_date = isset($config['active_date']) ? $config['active_date'] : '';
    $end_date = isset($config['end_date']) ? $config['end_date'] : '';
    $active_date = strtotime($active_date);
    $end_date = strtotime($end_date);
    $current_date = strtotime(date("Y-m-d"));
    if($active_date == '') {
      $active_date = strtotime(date("Y-m-d"));
    }
    if (($active_date >= $current_date) || ($end_date >= $current_date)) {
      $image_file_load = file_load($image_fid[0]);
      $flash_file_load = file_load($flash_fid[0]);
      if ($image_file_load != '') {
        $path = $image_file_load->getFileUri();
        $image = \Drupal\image\Entity\ImageStyle::load('medium')->buildUrl($path);
      }else{
        $image = '';
      }
      if ($flash_file_load != '') {
        $flash = $flash_file_load->getFileUri();
        $flash = file_create_url($flash);
      }else{
        $flash = '';
      }
      $simpleads[] = array(
        '#theme' => 'advertisement',
        '#ad_type' => $ad_type,
        '#image' => $image,
        '#url' => $url,
        '#text' => $text,
        '#flash' => $flash,
      );
    } else {
      $simpleads[] = array(
        '#theme' => 'advertisement',
        '#ad_type' => $ad_type,
      );
    }
    return $simpleads;
  }
}