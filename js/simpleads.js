(function ($, Drupal, window) {
    $(document).ready(function () {
        var ad_type = $("input[name='settings[ad_type]']:checked").val();
        if(ad_type == 'image') {
            $("input[name='files[settings_image]']").show();
            $(".form-item-settings-image").show();
        }else{
            $("input[name='files[settings_image]']").hide();
            $(".form-item-settings-image").hide();
        }
        if(ad_type == 'flash') {
            $("input[name='files[settings_flash]']").show();
            $(".form-item-settings-flash").show();
        }else{
            $("input[name='files[settings_flash]']").hide();
            $(".form-item-settings-flash").hide();
        }

        $("input[name='settings[ad_type]']").change(function(){
            var ad_type = $("input[name='settings[ad_type]']:checked").val();
            if(ad_type == 'image') {
                $("input[name='files[settings_image]']").show();
                $(".form-item-settings-image").show();
            }else{
                $("input[name='files[settings_image]']").hide();
                $(".form-item-settings-image").hide();
            }
            if(ad_type == 'flash') {
                $("input[name='files[settings_flash]']").show();
                $(".form-item-settings-flash").show();
            }else{
                $("input[name='files[settings_flash]']").hide();
                $(".form-item-settings-flash").hide();
            }
        });
    });
})(jQuery, Drupal, window);
